<?php

namespace App\Http\Controllers;

use App\Models\guru;
use App\Models\pembimbing;
use App\Models\perusahaan;
use App\Models\jurusan;
use App\Models\laporan;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
    public function index()
    {
        $jurusan = jurusan::all()->sortBy('name');
        return view('Admin.dashboard', [
            'jurusan' => $jurusan
        ]);
    }
    public function formupdate($id)
    {
        $data = User::all()->where('id', $id)->first();
        $data1 = guru::all();
        $data2 = pembimbing::all();

        return view('Admin.formEditSiswa', [
            'data' => $data,
            'data1' => $data1,
            'data2' => $data2
        ]);
    }
    public function update(Request $request)
    {
        $request->user()->where('id', $request->id)->update([
            'name' => $request->name,
            'nis' => $request->nis,
            'kehadiran' => $request->kehadiran,
            'status' => $request->status,
            'guru_id' => $request->guru_id,
            'pembimbing_id' => $request->pembimbing_id,
            'comentP' => $request->comentP,
            'comentS' => $request->comentS,
            'username' => $request->username,
            'email' => $request->email
        ]);
        return redirect('/home');
    }
    public function pembimbingTable()
    {
        return view('Admin.pembimbingTables');
    }
    public function createAccount()
    {
        $data1 = guru::all();
        return view('Admin.createAccount', [
            'data1' => $data1,
        ]);
    }
    public function insert(Request $request)
    {

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('foto');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'foto_users';
        $file->move($tujuan_upload, $nama_file);
        $request->user()->insert([
            'level' => $request->level,
            'name' => $request->name,
            'nis' => $request->nis,
            'kelas' => $request->kelas,
            'jurusan_id' => $request->jurusan,
            'foto_users' => $nama_file,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'created_at' => now()
        ]);
        if ($request->level == 'pembimbing') {
            DB::table('pembimbings')->insert([
                'name' => $request->name,
                'nip' => $request->nis,
                'jurusan_id' => $request->jurusan,
                'foto' => $nama_file,
                'created_at' => now()
            ]);
        }
        return redirect('/table');
    }
    public function delete($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect('/siswaTables');
    }
    public function insertPerusahaan(Request $request)
    {
        $jurusan = join(',', $request->input('jurusan'));
        $kerjasama = join(',', $request->input('kerjasama'));
        DB::table('perusahaans')->insert([
            'nama_perusahaan' => $request->nama_perusahaan,
            'alamat_perusahaan' => $request->alamat_perusahaan,
            'kejuruan' => $jurusan,
            'kerjasama' => $kerjasama
        ]);
        return redirect()->back();;
    }
    public function setting()
    {
        $data = jurusan::all();
        $data1 = laporan::all()->where('id', 1);
        return view('Admin.setting', [
            'data' => $data,
            'data1' => $data1
        ]);
    }
    public function insertJurusan(Request $request)
    {
        DB::table('jurusans')->insert([
            'jurusan' => $request->jurusan,
            'kepala_jurusan' => $request->kepala_jurusan
        ]);
        return redirect()->back();
    }
    public function updateJurusan(Request $request)
    {
        DB::table('jurusans')->where('id', $request->id)->update([
            'jurusan' => $request->jurusan,
            'kepala_jurusan' => $request->kepala_jurusan
        ]);
        return redirect()->back();
    }
    public function deleteJurusan($id)
    {
        DB::table('jurusans')->where('id', $id)->delete();
        return redirect()->back();
    }
    public function deletePerusahaan($id)
    {
        DB::table('perusahaans')->where('id', $id)->delete();
        return redirect()->back();
    }
}
